/*
Name:		CTALib.h
Created:	22/02/2017 14:26:31
Author:	puosm
Editor:	http://www.visualmicro.com
*/

#ifndef CTALib_h
#define CTALib_h

#include <PID_v1.h>
/*
#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif*/

#include <EEPROMex.h>
//#include "PID_one.h"


/*
* Classe Ventilateur
*/
class Ventilo {
public:
	PID myPID;
	byte startAddress;
	byte addressSize;
	byte pin_ON_OFF;
	byte pin_CMD;
	byte pin_DEFAUT;
	int consigne_m3h;
	double consigne_app_m3h;
	int cons_app_pc;
	double cons_app_pts;
	double Kp;
	double Ki;
	double Kd;
	double debit;
	int Kfactor;
	bool defaut;
	bool marche;
	bool changedPID;
	bool commandeMarche;

	int consigneDebitFreeCooling;
	int consigneDebitModeOccupe;
	int consigneDebitModeInoccupe;

	int consigneForcage;



	Ventilo(byte mypin_ON_OFF, byte mypin_CMD, byte mypin_DEFAUT, int K);
	void setAddress(byte add);
	void calculDebit(float pression);
	void update();
	void saveToEEPROM();
	void loadFromEEPROM();
};

/*
* Classe entr�e ANA
*/
class Pression {
public:
	byte pin;
	int value;
	Pression(byte myPin);
	int read();
};

/*
* Classe Registre Motorise
*/
class RM {
public:
	PID PID_soufflage;
	PID PID_reprise;
	byte startAddress;
	byte addressSize;
	byte pin_CMD;
	double mesureTempS;
	double consigneTempS;
	double cons_app_pcS;
	double cons_app_ptsS;
	double KpS;
	double KiS;
	double KdS;
	double mesureTempR;
	double consigneTempR;
	double cons_app_pcR;
	double cons_app_ptsR;
	double KpR;
	double KiR;
	double KdR;
	double consigneForcage;
	bool forcage;
	bool changedPID;

	RM(byte mypin_CMD);
	void update();
	void saveToEEPROM();
	void loadFromEEPROM();
};


#endif

