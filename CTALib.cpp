/*
Name:		CTALib.cpp
Created:	22/02/2017 14:26:31
Author:	puosm
Editor:	http://www.visualmicro.com
*/

#include "CTALib.h"

/*
* Classe Capteur de pression
*/

Pression::Pression(byte myPin) {
	pin = myPin;
	pinMode(pin, INPUT);
}
int Pression::read() {
	//mise a l'echelle: 0.5-4.5V ==> 0-300Pa sur 1023 pts: 	value = (0.3666*analogRead(pin) - 37.5);
	//mise a l'echelle: 0.5-4.5V ==> 0-800Pa sur 1023 pts: 	value = (0.9766*analogRead(pin) - 100);
	int volt = map(analogRead(pin), 0, 1024, 0, 100);// on multiplie par 10 pour ne pas avoir de float dans la fonction map
	value = map(volt, 5,45,0,1000);
	if (value<0) value = 0;
	if (value>1000) value = 1000;
	//Serial.print(value);
	return value;
}


/*
* Classe Ventilateur
*/

Ventilo::Ventilo(byte mypin_ON_OFF, byte mypin_CMD, byte mypin_DEFAUT, int K) :myPID(&debit, &cons_app_pts, &consigne_app_m3h, Kp, Ki, Kd, DIRECT) {
	pin_ON_OFF = mypin_ON_OFF;
	pin_CMD = mypin_CMD;
	pin_DEFAUT = mypin_DEFAUT;
	pinMode(pin_ON_OFF, OUTPUT);
	pinMode(pin_CMD, OUTPUT);
	pinMode(pin_DEFAUT, INPUT);
	defaut = false;
	marche = false;
	changedPID = false;
	Kfactor = K;
	Kp = 1;
	Ki = 0;
	Kd = 0;
	addressSize = 3 * sizeof(double) + 6 * sizeof(int);
}
void Ventilo::setAddress(byte add) {
	startAddress = add;
}
void Ventilo::calculDebit(float pression) {
	double d;
	if (pression >= 0) {
		d = Kfactor*sqrt(pression);
	}
	else d = 0;
	//if (d > 1.1*debit) d = 1.1*debit;
	//if (d < 0.9*debit) d = 0.9*debit;
	//debit = 0.95 * debit + d * 0.05;
	debit = d;	
}

void Ventilo::update() {
	digitalWrite(pin_ON_OFF, marche);
	cons_app_pc = map(cons_app_pts, 0, 255, 0, 100);
	analogWrite(pin_CMD, cons_app_pts);
}

void Ventilo::saveToEEPROM() {
	int address = startAddress;
	EEPROM.updateDouble(address, Kp); address += sizeof(double);
	EEPROM.updateDouble(address, Ki); address += sizeof(double);
	EEPROM.updateDouble(address, Kd); address += sizeof(double);
	EEPROM.updateInt(address, Kfactor); address += sizeof(int);
	EEPROM.updateInt(address, consigneDebitFreeCooling); address += sizeof(int);
	EEPROM.updateInt(address, consigneDebitModeOccupe); address += sizeof(int);
	EEPROM.updateInt(address, consigneDebitModeInoccupe); address += sizeof(int);
	EEPROM.updateInt(address, consigneForcage); address += sizeof(int);
	EEPROM.updateBit(address, 0, commandeMarche);
}
void Ventilo::loadFromEEPROM() {
	int address = startAddress;
	Kp = EEPROM.readDouble(address); address += sizeof(double);
	Ki = EEPROM.readDouble(address); address += sizeof(double);
	Kd = EEPROM.readDouble(address); address += sizeof(double);
	Kfactor = EEPROM.readInt(address); address += sizeof(int);
	consigneDebitFreeCooling = EEPROM.readInt(address); address += sizeof(int);
	consigneDebitModeOccupe = EEPROM.readInt(address); address += sizeof(int);
	consigneDebitModeInoccupe = EEPROM.readInt(address); address += sizeof(int);
	consigneForcage = EEPROM.readInt(address); address += sizeof(int);
	commandeMarche = EEPROM.readBit(address, 0);
}

/*
* Classe Registre Motorise
*/
/*
RM::RM(byte mypin_CMD) :PID_soufflage(&mesureTempS, &cons_app_ptsS, &consigneTempS, KpS, KiS, KdS, REVERSE), PID_reprise(&mesureTempR, &cons_app_ptsR, &consigneTempR, KpR, KiR, KdR, REVERSE) {
	pin_CMD = mypin_CMD;
	pinMode(pin_CMD, OUTPUT);
	changedPID = false;
	KpS = 10;
	KiS = 0;
	KdS = 0;
	KpR = 10;
	KiR = 0;
	KdR = 0;
	forcage = false;
	addressSize = 6 * sizeof(double) + 2 * sizeof(int);
}
void RM::update() {
	cons_app_pcS = (int)(-cons_app_ptsS*cons_app_ptsS*cons_app_ptsS*0.000005 + 0.0029*cons_app_ptsS*cons_app_ptsS - 0.8734*cons_app_ptsS + 112.94);
	analogWrite(pin_CMD, cons_app_ptsS);
	cons_app_pcR = (int)(-cons_app_ptsR*cons_app_ptsR*cons_app_ptsR*0.000005 + 0.0029*cons_app_ptsR*cons_app_ptsR - 0.8734*cons_app_ptsR + 112.94);
	if (cons_app_pcS > 97) cons_app_pcS = 100;
	if (cons_app_pcR > 97) cons_app_pcR = 100;
	if (cons_app_pcS < 0) cons_app_pcS = 0;
	if (cons_app_pcR < 0) cons_app_pcR = 0;

}
void RM::saveToEEPROM() {
	int address = startAddress;
	EEPROM.updateDouble(address, KpS); address += sizeof(double);
	EEPROM.updateDouble(address, KiS); address += sizeof(double);
	EEPROM.updateDouble(address, KdS); address += sizeof(double);
	EEPROM.updateDouble(address, KpR); address += sizeof(double);
	EEPROM.updateDouble(address, KiR); address += sizeof(double);
	EEPROM.updateDouble(address, KdR); address += sizeof(double);
	EEPROM.updateInt(address, consigneForcage); address += sizeof(int);
	EEPROM.updateBit(address, 0, forcage);
}
void RM::loadFromEEPROM() {
	int address = startAddress;
	KpS = EEPROM.readDouble(address); address += sizeof(double);
	KiS = EEPROM.readDouble(address); address += sizeof(double);
	KdS = EEPROM.readDouble(address); address += sizeof(double);
	KpR = EEPROM.readDouble(address); address += sizeof(double);
	KiR = EEPROM.readDouble(address); address += sizeof(double);
	KdR = EEPROM.readDouble(address); address += sizeof(double);
	consigneForcage = EEPROM.readInt(address); address += sizeof(int);
	forcage = EEPROM.readBit(address, 0);
}*/
