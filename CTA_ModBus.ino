﻿

//#include "PID_one.h"
#include <DallasTemperature.h>
#include <OneWire.h>
#include <EEPROMVar.h>
#include <EEPROMex.h>
#include <PID_v1.h>
#include "genieArduino.h"
#include "CTALib.h"
#include "CONTROLLINO.h"
#include <Modbus.h>
#include <SPI.h>
#include <Ethernet.h>
#include <ModbusIP.h>



// VARIABLES GLOBALES

#define memoryBase 32

//PIN LAYOUT
#define pin_DEFAUT_V1 11                   //V1= REPRISE V2= SOUFF			//A check 
#define pin_CMD_RM1 7
#define pin_ONOFF_V1 6
#define pin_ONOFF_V2 5
#define pin_CMD_V1 12
#define pin_TEMPERATURE A13 // digitalIn
#define pin_DEFAUT_V2 10
#define pin_CMD_V2 13
#define pin_P1 A15		//reprise
#define pin_P2 A14		//souff
#define RESETLINE 45
#define BP_MARCHE A10
#define BP_ARRET A11
#define POTAR A0
#define VOYANT_MARCHE 3
#define VOYANT_DEFAUT 2

int codeAdmin = 9876;

/*TEMPERATURES*/
OneWire ds(pin_TEMPERATURE); //Cr?ation de l'objet OneWire pour manipuler le bus 1-Wire
DallasTemperature sensors(&ds);
int SOUFFLAGE = 0;
int REPRISE = 1;
int EXTRACTION = 2;
int AIRNEUF = 3;

int seuilTempHorsGel = 5;
int debitMaxCTA = 1000;
int consigneTempSoufflageEte = 16;
int consigneTempSoufflageHiver = 30;
bool regulTempReprise = false; // true = regul sur temperature soufflage; false = regul sur temperature soufflage
bool changeOver = false; //true = ete; false = hiver
bool occupe = false; //true = Occupe; false = inoccupe

bool KB = false;
bool changedParams = false;

int heure;
int minute;
int annee;
int mois;
int jour;
int jourSemaine;

int heuremodif;
int minutemodif;
int anneemodif;
int moismodif;
int jourmodif;
int jourSemainemodif;

typedef struct {
	float value;
	byte addr[8];
	byte status;
}temperatureDS;
temperatureDS temperature[4];

/* Code de retour de la fonction getTemperature() */
enum DS18B20_RCODES {
	READ_OK,
	NO_SENSOR_FOUND,
	INVALID_ADDRESS,
	INVALID_SENSOR
};

/*
* ECRAN 4D SYSTEMS
*/
Genie genie;
static long waitPeriod;
static long waitPeriodTemps;
static long waitPeriodDate;
double *ptr;
int *int_ptr;
String * string_ptr;

int int_KBmin, int_KBmax;
double double_KBmin, double_KBmax;

bool readIntFromKB = false;
bool readStrFromKB = false;
bool readHeureFromKB = false;
bool erreurKB = false;
bool bpmarche = false;
bool bparret = false;
bool ecranbpmarche = false;
bool ecranbparret = false;
bool Ventilodefaut = false;
bool RegistreONOFF = false;
bool Registreforcage = false;
bool savedate = false;
bool modifdate = false;

int consigne_m3h;

String str = "";
String param = "";
String versioncontrollino = "CTA_ModBus.ino";
bool FormTemperatureActivated = false;
enum FORM {
	ACCUEIL = 0,
	MENU = 1,
	MENUADMIN = 2,
	SYNOPTIQUE = 3,
	PARAMS_VENTILO = 4,
	KEYBOARD = 5,
	PARAMS_CTA = 6,
	PARAMS_CTA2 = 7,
	CONFIG_CTA = 8,
	PARAMS_RM = 9,
	BOUTONS = 10,
	PARAMS_CTA3 = 11,
	DATE_ET_HEURE = 12
};

FORM form = ACCUEIL;
/*
* PARAMETRES CTA
*/
enum modeMarche {
	ARRET,
	MANU,
	AUTO,
	CMDBOUTONS
};

enum modeHE {
	HIVER_OCC,
	HIVER_INOCC,
	ETE_OCC,
	ETE_INOCC
};

enum mode {
	NORMAL,
	FREECOOLING,
	FREEHEATING,
	HORSGEL  // pas forcement 
};
modeHE modeHE_Applique = HIVER_OCC;
modeMarche modeMarche_Applique = ARRET;
mode modeApplique = NORMAL;
int consignesTemp[4];

int heureDebutOccupe;
int heureFinOccupe;
bool jourOccupe[7];
int jourOccupe_int;

/*
* EBM PAPST ROTOR Dimension: 250 / 280 / 310 / 355 / 400 / 450 / 500 / 560 / 630 / 710 / 800 / 900
*                  K-FACTOR:  76 / 77 / 116 / 148 / 188 / 240 / 281 / 348 / 438 / 545 / 695 / 900
*/

Ventilo V2(pin_ONOFF_V2, pin_CMD_V2, pin_DEFAUT_V2, 240);
Ventilo V1(pin_ONOFF_V1, pin_CMD_V1, pin_DEFAUT_V1, 240);

Pression P1(pin_P1);
Pression P2(pin_P2);
//RM RM1(pin_CMD_RM1);

//ModbusIP object
ModbusIP mb;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte ip[] = { 192, 168, 1, 55 };
byte mydns[] = { 8, 8, 8, 8 };
byte gw[] = { 192, 168, 1, 1 };
String adresseIP, passerelle;


unsigned long debutTempoModbus = 0;
unsigned long tempoModbus = 500;

void setModbus() {
	//Config Modbus IP
	mb.config(mac, ip, mydns, gw);
	mb.addHreg(100);
	mb.addCoil(101);
	mb.addCoil(102);
	mb.addHreg(103);
	mb.addHreg(104);
	mb.addHreg(105);
	mb.addHreg(106);
	mb.addHreg(107);
	mb.addHreg(108);
	mb.addHreg(109);
	mb.addHreg(110);
	mb.addHreg(111);
	mb.addCoil(112);
	mb.addHreg(113);
	mb.addHreg(114);
	mb.addHreg(115);
	mb.addHreg(116);
	mb.addHreg(117);
	mb.addHreg(118);
	mb.addHreg(119);
	mb.addHreg(120);
	mb.addCoil(121);
	mb.addHreg(122);
	mb.addCoil(123);
	mb.addHreg(124);
	mb.addHreg(125);
	mb.addHreg(126);
	mb.addHreg(127);
	mb.addHreg(128);
	mb.addHreg(129);
	mb.addHreg(130);
	mb.addHreg(131);
	mb.addCoil(132);
	mb.addHreg(133);
	mb.addCoil(134);
	mb.addHreg(135);
	mb.addHreg(136);
	mb.addHreg(137);
	mb.addHreg(138);
	mb.addHreg(139);
	mb.addHreg(140);
	mb.addHreg(141);
	mb.addHreg(142);
	mb.addHreg(143);
	mb.addHreg(144);
	mb.addHreg(145);
	mb.addHreg(146);
	mb.addHreg(147);
	mb.addHreg(148);
	mb.addHreg(149);
	mb.addHreg(150);
	mb.addHreg(151);
	mb.addHreg(152);
	mb.addHreg(153);
	mb.addHreg(154);
}

void readFromGTC() {
	modeMarche_Applique = (modeMarche)mb.Hreg(100);
	changeOver = (bool)mb.Coil(101);
	occupe = (bool)mb.Coil(102);
	debitMaxCTA = mb.Hreg(103);
	seuilTempHorsGel = mb.Hreg(104);
	consignesTemp[HIVER_OCC] = mb.Hreg(105);
	consignesTemp[HIVER_INOCC] = mb.Hreg(106);
	consignesTemp[ETE_OCC] = mb.Hreg(107);
	consignesTemp[ETE_INOCC] = mb.Hreg(108);
	consigneTempSoufflageEte = mb.Hreg(109);
	consigneTempSoufflageHiver = mb.Hreg(110);
	regulTempReprise = (bool)mb.Coil(112);
	V1.consigneDebitFreeCooling = mb.Hreg(113);
	V1.consigneDebitModeOccupe = mb.Hreg(114);
	V1.consigneDebitModeInoccupe = mb.Hreg(115);
	V1.Kp = (double)(mb.Hreg(117)) / 100;
	V1.Ki = (double)(mb.Hreg(118)) / 100;
	V1.Kd = (double)(mb.Hreg(119)) / 100;
	V1.Kfactor = mb.Hreg(120);
	V1.marche = (bool)mb.Coil(121);
	V2.consigneDebitFreeCooling = mb.Hreg(124);
	V2.consigneDebitModeOccupe = mb.Hreg(125);
	V2.consigneDebitModeInoccupe = mb.Hreg(126);
	V2.Kp = (double)(mb.Hreg(128)) / 100;
	V2.Ki = (double)(mb.Hreg(129)) / 100;
	V2.Kd = (double)(mb.Hreg(130)) / 100;
	V2.Kfactor = mb.Hreg(131);
	V2.marche = (bool)mb.Coil(132);
/*	RM1.KpS = (double)(mb.Hreg(138)) / 100;
	RM1.KiS = (double)(mb.Hreg(139)) / 100;
	RM1.KdS = (double)(mb.Hreg(140)) / 100;
	RM1.KpR = (double)(mb.Hreg(144)) / 100;
	RM1.KiR = (double)(mb.Hreg(145)) / 100;
	RM1.KdR = (double)(mb.Hreg(146)) / 100;*/
	heure = mb.Hreg(151);
	minute = mb.Hreg(150);
	annee = mb.Hreg(149);
	mois = mb.Hreg(148);
	jour = mb.Hreg(147);
	jourSemaine = mb.Hreg(152);
	heureDebutOccupe = mb.Hreg(154);
	heureFinOccupe = mb.Hreg(153);
}


void writeToGTC_variables() {
//	mb.Hreg(111, RM1.consigneTempS);
	mb.Hreg(116, V1.consigne_app_m3h);
	mb.Hreg(122, V1.cons_app_pc);
	mb.Coil(123, V1.defaut);
	mb.Hreg(127, V2.consigne_app_m3h);
	mb.Hreg(133, V2.cons_app_pc);
	mb.Coil(134, V2.defaut);
/*	mb.Hreg(135, RM1.consigneTempS);
	mb.Hreg(136, RM1.mesureTempS);
	mb.Hreg(137, RM1.cons_app_pcS);
	mb.Hreg(141, RM1.consigneTempR);
	mb.Hreg(142, RM1.mesureTempR);
	mb.Hreg(143, RM1.cons_app_pcR);*/
}

void writeToGTC_params() {
	mb.Hreg(100, modeMarche_Applique);
	mb.Coil(101, changeOver);
	mb.Coil(102, occupe);
	mb.Hreg(103, debitMaxCTA);
	mb.Hreg(104, seuilTempHorsGel);
	mb.Hreg(105, consignesTemp[HIVER_OCC]);
	mb.Hreg(106, consignesTemp[HIVER_INOCC]);
	mb.Hreg(107, consignesTemp[ETE_OCC]);
	mb.Hreg(108, consignesTemp[ETE_INOCC]);
	mb.Hreg(109, consigneTempSoufflageEte);
	mb.Hreg(110, consigneTempSoufflageHiver);
	mb.Coil(112, regulTempReprise);
	mb.Hreg(113, V1.consigneDebitFreeCooling);
	mb.Hreg(114, V1.consigneDebitModeOccupe);
	mb.Hreg(115, V1.consigneDebitModeInoccupe);
	mb.Hreg(117, (word)(V1.Kp * 100));
	mb.Hreg(118, (word)(V1.Ki * 100));
	mb.Hreg(119, (word)(V1.Kd * 100));
	mb.Hreg(120, V1.Kfactor);
	mb.Coil(121, V1.marche);
	mb.Hreg(124, V2.consigneDebitFreeCooling);
	mb.Hreg(125, V2.consigneDebitModeOccupe);
	mb.Hreg(126, V2.consigneDebitModeInoccupe);
	mb.Hreg(128, (word)(V2.Kp * 100));
	mb.Hreg(129, (word)(V2.Ki * 100));
	mb.Hreg(130, (word)(V2.Kd * 100));
	mb.Hreg(131, V2.Kfactor);
	mb.Coil(132, V2.marche);
/*	mb.Hreg(138, (word)(RM1.KpS * 100));
	mb.Hreg(139, (word)(RM1.KiS * 100));
	mb.Hreg(140, (word)(RM1.KdS * 100));
	mb.Hreg(144, (word)(RM1.KpR * 100));
	mb.Hreg(145, (word)(RM1.KiR * 100));
	mb.Hreg(146, (word)(RM1.KdR * 100));*/
	mb.Hreg(154, heureDebutOccupe);
	mb.Hreg(153, heureFinOccupe);
}


void modbus() {
	mb.task();
	if (millis() - debutTempoModbus >= tempoModbus) {
		debutTempoModbus = millis();
		writeToGTC_variables();
		readFromGTC();
		saveCTA(0);
	}
}

void loadCTA(int address) {
	SOUFFLAGE = EEPROM.readInt(address); address += sizeof(int);
	REPRISE = EEPROM.readInt(address); address += sizeof(int);
	EXTRACTION = EEPROM.readInt(address); address += sizeof(int);
	AIRNEUF = EEPROM.readInt(address); address += sizeof(int);
	seuilTempHorsGel = EEPROM.readDouble(address); address += sizeof(double);
	modeHE_Applique = (modeHE)EEPROM.readInt(address); address += sizeof(int);
	modeMarche_Applique = (modeMarche)EEPROM.readInt(address); address += sizeof(int);
	changeOver = EEPROM.readInt(address); address += sizeof(int);
	consignesTemp[0] = EEPROM.readDouble(address); address += sizeof(double);
	consignesTemp[1] = EEPROM.readDouble(address); address += sizeof(double);
	consignesTemp[2] = EEPROM.readDouble(address); address += sizeof(double);
	consignesTemp[3] = EEPROM.readDouble(address); address += sizeof(double);
	debitMaxCTA = EEPROM.readDouble(address); address += sizeof(double);
	consigneTempSoufflageEte = EEPROM.readDouble(address); address += sizeof(double);
	consigneTempSoufflageHiver = EEPROM.readDouble(address); address += sizeof(double);
	//regulTempReprise = EEPROM.readBit(address, 0);
	//occupe = EEPROM.readBit(address, 2);
	for (int i = 0; i < 7; i++) {
		jourOccupe[i] = EEPROM.readBit(address, i + 3);
	}
	address += sizeof(int);

	heureDebutOccupe = EEPROM.readInt(address); address += sizeof(int);
	heureFinOccupe = EEPROM.readInt(address); address += sizeof(int);
	V1.startAddress = address;
	address += V1.addressSize;
	V2.startAddress = address;
	address += V2.addressSize;
	V1.loadFromEEPROM();
	V2.loadFromEEPROM();
	//IPToString();
}

void saveCTA(int address) {
	EEPROM.updateInt(address, SOUFFLAGE); address += sizeof(int);
	EEPROM.updateInt(address, REPRISE); address += sizeof(int);
	EEPROM.updateInt(address, EXTRACTION); address += sizeof(int);
	EEPROM.updateInt(address, AIRNEUF); address += sizeof(int);
	EEPROM.updateDouble(address, seuilTempHorsGel); address += sizeof(double);
	EEPROM.updateInt(address, modeHE_Applique); address += sizeof(int);
	EEPROM.updateInt(address, modeMarche_Applique); address += sizeof(int);
	EEPROM.updateInt(address, changeOver); address += sizeof(int);
	EEPROM.updateDouble(address, consignesTemp[0]); address += sizeof(double);
	EEPROM.updateDouble(address, consignesTemp[1]); address += sizeof(double);
	EEPROM.updateDouble(address, consignesTemp[2]); address += sizeof(double);
	EEPROM.updateDouble(address, consignesTemp[3]); address += sizeof(double);
	EEPROM.updateDouble(address, debitMaxCTA); address += sizeof(double);
	EEPROM.updateDouble(address, consigneTempSoufflageEte); address += sizeof(double);
	EEPROM.updateDouble(address, consigneTempSoufflageHiver); address += sizeof(double);
	//EEPROM.updateBit(address, regulTempReprise, 0);
	Serial.print(changeOver);

	//EEPROM.updateBit(address, occupe, 2);
	for (int i = 0; i < 7; i++) {
		EEPROM.updateBit(address, jourOccupe[i], i + 3);
	}
	address += sizeof(int);

	EEPROM.updateInt(address, heureDebutOccupe); address += sizeof(int);
	EEPROM.updateInt(address, heureFinOccupe); address += sizeof(int);
	address += V1.addressSize;
	address += V2.addressSize;
	V1.saveToEEPROM();
	V2.saveToEEPROM();
}

void initCTA() {

	loadCTA(0);
	V2.myPID.SetMode(AUTOMATIC);
	V2.myPID.SetOutputLimits(0, 255);
	V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
	V1.myPID.SetMode(AUTOMATIC);
	V1.myPID.SetOutputLimits(0, 255);
	V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);

}
void RTCmiseajour() {
	if (modifdate == true) {
		Controllino_RTC_init(0);
		Controllino_SetTimeDate(jour, jourSemaine, mois, annee, heure, minute, 12);
		delay(10);
		modifdate = false;
		return;
	}
	jour = Controllino_GetDay();
	jourSemaine = Controllino_GetWeekDay();
	minute = Controllino_GetMinute();
	heure = Controllino_GetHour();
	mois = Controllino_GetMonth();
	annee = Controllino_GetYear();


}

void setup() {
	EEPROM.setMemPool(memoryBase, EEPROMSizeNano); //Set memorypool base to 32, assume Arduino Uno board
	pinMode(pin_TEMPERATURE, INPUT);
	Serial.begin(9600);
	Serial1.begin(9600);
	Serial.println("START1");
	sensors.begin();
	initCTA();
	Controllino_RTC_init(0);
	startScreen();
	delay(1000);
	genie.WriteObject(GENIE_OBJ_FORM, FORM::MENU, 0);
	form = MENU;
	RegistreONOFF = false;
	Serial.println("START");
	//Serial.print("device count:");
	//Serial.println(sensors.getDeviceCount());
	getDS18B20Addresses(4);
	delay(200);

	setModbus();
	delay(1000);
}

void setModeHE() {
	if (changeOver) {
		if (occupe) modeHE_Applique = ETE_OCC;
		else modeHE_Applique = ETE_INOCC;
	}
	else {
		if (occupe) modeHE_Applique = HIVER_OCC;
		else modeHE_Applique = HIVER_INOCC;
	}
}

void setOccupation() {
	int heurecourante = heure * 100 + minute;
	if (jourOccupe[jourSemaine - 1]) {
		if (heurecourante >= heureDebutOccupe && heurecourante <= heureFinOccupe) {
			occupe = true;
			return;
		}
	}
	occupe = false;
}

void setMode() {
	if (temperature[AIRNEUF].value < seuilTempHorsGel) {
		modeApplique = HORSGEL;
		//RegistreONOFF = true;  //Ouverture Registre
		return;
	}
	if (modeHE_Applique == HIVER_OCC || modeHE_Applique == HIVER_INOCC) {
		if (temperature[AIRNEUF].value >= consigneTempSoufflageHiver) { //MODE HIVER
			modeApplique = FREEHEATING;
			RegistreONOFF = true;  //Ouverture Registre
			return;
		}
	}
	if (modeHE_Applique == ETE_OCC || modeHE_Applique == ETE_INOCC) {
		if (temperature[AIRNEUF].value <= consigneTempSoufflageEte) { //MODE ETE
			modeApplique = FREECOOLING;
			RegistreONOFF = true;  //Ouverture Registre
			return;
		}
	}

	modeApplique = NORMAL;
	RegistreONOFF = false;
	if (RegistreONOFF == false && Registreforcage == true) RegistreONOFF = true;
	return;
}

void regulDebit() {
	V1.defaut = !digitalRead(pin_DEFAUT_V1);
	V2.defaut = !digitalRead(pin_DEFAUT_V2);			// enlever les ! si contacte NO
	if (V1.defaut || V2.defaut) {
		digitalWrite(VOYANT_DEFAUT, HIGH);
		Ventilodefaut = true;
	}
	else {
		Ventilodefaut = false;
		digitalWrite(VOYANT_DEFAUT, LOW);
	}

	if (modeMarche_Applique == ARRET) {
		V1.marche = false;
		V2.marche = false;
		//RegistreONOFF = false;
		digitalWrite(VOYANT_MARCHE, LOW);
	}
	else if (modeMarche_Applique == MANU) {
		V1.marche = V1.commandeMarche;
		V2.marche = V2.commandeMarche;
		V1.consigne_m3h = V1.consigneDebitModeOccupe;
		V2.consigne_m3h = V2.consigneDebitModeOccupe;

	}
	else if (modeMarche_Applique == AUTO) {
		V1.marche = V1.commandeMarche;
		V2.marche = V2.commandeMarche;
		if (modeApplique == FREECOOLING || modeApplique == FREEHEATING) { //FREECOOLING ou FREEHEATING
			V1.consigne_m3h = V1.consigneDebitFreeCooling;
			V2.consigne_m3h = V2.consigneDebitFreeCooling;
		}
		else {
			if (!occupe) {
				V1.consigne_m3h = V1.consigneDebitModeInoccupe;
				V2.consigne_m3h = V2.consigneDebitModeInoccupe;
			}
			else {
				V1.consigne_m3h = V1.consigneDebitModeOccupe;
				V2.consigne_m3h = V2.consigneDebitModeOccupe;
			}
		}
	}
	else {
		if (bpmarche == 1) {
			V1.marche = true;
			V2.marche = true;
			consigne_m3h = map(analogRead(POTAR), 0, 850, 0, debitMaxCTA);
			V2.consigne_m3h = consigne_m3h;
			V1.consigne_m3h = consigne_m3h;


			bpmarche = 1;

		}
		else {
			V1.marche = false;
			V2.marche = false;
			consigne_m3h = map(analogRead(POTAR), 0, 850, 0, debitMaxCTA);
		}
	}

	digitalWrite(pin_ONOFF_V1, V1.marche);
	digitalWrite(pin_ONOFF_V2, V2.marche);
	digitalWrite(VOYANT_MARCHE, bpmarche);


	if (V1.marche) {
		V1.consigne_app_m3h = V1.consigne_m3h;
		V1.myPID.Compute();



	}
	else {
		V1.consigne_app_m3h = -1000;
		V1.cons_app_pts = 0;
	}
	V1.update();


	if (V2.marche) {
		V2.consigne_app_m3h = V2.consigne_m3h;
		V2.myPID.Compute();
	}
	else {
		V2.consigne_app_m3h = -1000;
		V2.cons_app_pts = 0;

	}

	V2.update();

	if (RegistreONOFF == false) digitalWrite(pin_CMD_RM1, LOW);
	else 	digitalWrite(pin_CMD_RM1, HIGH);

}
void ModeBoutons(void) {

	if (analogRead(BP_ARRET) > 100 || ecranbparret == true) {
		bpmarche = false;
		bparret = true;
		ecranbparret = false;
		return;
	}
	else if (ecranbpmarche == true || analogRead(BP_MARCHE) > 100) {
		bpmarche = true;
		bparret = false;
		ecranbpmarche = false;
		return;
	}
	else if (bpmarche == true) {
		bpmarche = true;
		bparret = false;
		return;
	}
	else if (bparret == true) {
		bpmarche = false;
		bparret = true;
		return;
	}
}

void loop() {
	modbus();
	genie.DoEvents(); // Gestion des evenements de l'ecran		  
	regulDebit();
	ModeBoutons();
	V1.calculDebit(P1.read());
	V2.calculDebit(P2.read());
	//Serial.print("changeover=");
	//Serial.print(changeOver);
	//Serial.print("\r\ndebit=");
	//Serial.print(V1.debit);
	//Serial.print("\r\ndebit=");

	if (millis() >= waitPeriod)
	{
		writeScreen();
		setOccupation();
		setMode();
		setModeHE();
		waitPeriod = millis() + 500;
	}
	if (millis() >= waitPeriodTemps)
	{
		readMesures();
		waitPeriodTemps = millis() + 10000;
	}
	if (millis() >= waitPeriodDate)
	{
		RTCmiseajour();
		waitPeriodDate = millis() + 30000;
	}
}

void myGenieEventHandler(void)
{

	//Serial.println("ok");
	int keyboardValue;
	genieFrame Event;
	genie.DequeueEvent(&Event); // Remove the next queued event from the buffer, and process it below

								//If the cmd received is from a Reported Event (Events triggered from the Events tab of Workshop4 objects)
	if (Event.reportObject.cmd == GENIE_REPORT_EVENT)
	{
		waitPeriod = 0;
		if (Event.reportObject.object == GENIE_OBJ_4DBUTTON) // If this event is from a 4DButton
		{
			switch (Event.reportObject.index) {
			case 0: //MARCHEV1
				if (Event.reportObject.data_lsb == 0) V1.commandeMarche = false;
				else V1.commandeMarche = true;
				break;
			case 5: //MARCHEV2
				if (Event.reportObject.data_lsb == 0) V2.commandeMarche = false;
				else V2.commandeMarche = true;
				break;
			case 2: //FORCAGE BYPASS
				if (Event.reportObject.data_lsb == 0) Registreforcage = false;
				else Registreforcage = true;

				break;
			case 3: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) changeOver = false;
				else changeOver = true;
				break;
			case 4: //MARCHEV1
				ecranbparret = true;
				break;
			case 1: //MARCHEV1
				ecranbpmarche = true;
				break;
			}
			changedParams = true;
			saveCTA(0);
		}
		if (Event.reportObject.object == GENIE_OBJ_DIPSW) // If this event is from a DIPSWITCH
		{
			switch (Event.reportObject.index) {
			case 0: //MODE MARCHE
				if (Event.reportObject.data_lsb == 0) modeMarche_Applique = modeMarche::ARRET;
				else if (Event.reportObject.data_lsb == 1) modeMarche_Applique = modeMarche::MANU;
				else if (Event.reportObject.data_lsb == 2) modeMarche_Applique = modeMarche::AUTO;
				else modeMarche_Applique = modeMarche::CMDBOUTONS;
				break;
			case 1: //GESTION MODE OCCUPE
				if (Event.reportObject.data_lsb == 0) jourOccupe[0] = false;
				else jourOccupe[0] = true;
				break;
			case 2:
				if (Event.reportObject.data_lsb == 0) jourOccupe[1] = false;
				else jourOccupe[1] = true;
				break;
			case 3:
				if (Event.reportObject.data_lsb == 0) jourOccupe[2] = false;
				else jourOccupe[2] = true;
				break;
			case 4:
				if (Event.reportObject.data_lsb == 0) jourOccupe[3] = false;
				else jourOccupe[3] = true;
				break;
			case 5:
				if (Event.reportObject.data_lsb == 0) jourOccupe[4] = false;
				else jourOccupe[4] = true;
				break;
			case 6:
				if (Event.reportObject.data_lsb == 0) jourOccupe[5] = false;
				else jourOccupe[5] = true;
				break;
			case 7:
				if (Event.reportObject.data_lsb == 0) jourOccupe[6] = false;
				else jourOccupe[6] = true;
				break;
			}
			changedParams = true;
			for (int i = 0; i < 7; i++) {
				bitWrite(jourOccupe_int, i, jourOccupe[i]);
			}
		}

		if (Event.reportObject.object == GENIE_OBJ_WINBUTTON) // If this event is from a WinButton
		{
			//Serial.println("EVENT");
			//Serial.print(Event.reportObject.object);//////////////////////////////
			str = "";
			switch (Event.reportObject.index) {
			case 5: //FORM MENUADMIN ==>Clavier pour mode admin
				form = FORM::MENUADMIN;
				param = "mot de passe admin";
				genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
				break;
			case 20: //FORM MENUADMIN ==>Retour
			case 30: //FORM MENUADMIN ==>Retour
			case 19: //FORM MENUADMIN ==>Retour
				form = FORM::MENUADMIN;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 0: //FORM Boite a boutons 
				form = FORM::BOUTONS;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 2: //FORM SYNOPTIQUE
				form = FORM::SYNOPTIQUE;
				waitPeriodTemps = 0;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 4: //FORM CONFIG CTA
				form = FORM::CONFIG_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 16: //FORM Params Ventilateurs
				form = FORM::PARAMS_VENTILO;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 17: //FORM Params Registre
				form = FORM::PARAMS_RM;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 21: //modif de la date et l'heure
				form = FORM::DATE_ET_HEURE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 3: //FORM Params CTA
			case 46: //FORM Params CTA
				form = FORM::PARAMS_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 52: //FORM Params CTA2
			case 40: //FORM Params CTA2
				form = FORM::PARAMS_CTA2;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 47: //FORM Params CTA3
				form = FORM::PARAMS_CTA3;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 18: //FORM MENU
			case 36: //FORM MENU
			case 41: //FORM MENU
			case 51: //FORM MENU
			case 7: //FORM MENU
			case 22: //FORM MENU	
			case 1: //FORM MENU	
				form = MENU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
				//Modifs de params au clavier
			case 6:
				processToKeyboard(&seuilTempHorsGel, "seuil T?C Hors Gel", 0, 50);
				break;
			case 8:
				processToKeyboard(&V1.Kp, "Kp Ventilo Soufflage", 0, 1000);
				break;
			case 9:
				processToKeyboard(&V1.Ki, "Ki Ventilo Soufflage", 0, 1000);
				break;
			case 10:
				processToKeyboard(&V1.Kd, "Kd Ventilo Soufflage", 0, 1000);
				break;
			case 11:
				processToKeyboard(&V1.Kfactor, "Kfactor Ventilo Soufflage", 0, 1000);
				break;
			case 12:
				processToKeyboard(&V2.Kp, "Kp Ventilo Reprise", 0, 1000);
				break;
			case 13:
				processToKeyboard(&V2.Ki, "Ki Ventilo Reprise", 0, 1000);
				break;
			case 14:
				processToKeyboard(&V2.Kd, "Kd Ventilo Reprise", 0, 1000);
				break;
			case 15:
				processToKeyboard(&V2.Kfactor, "Kfactor Ventilo Reprise", 0, 1000);
				break;
			case 23:
				processToKeyboard(&jourSemaine, "Jour de la semaine 1:Lundi 2:Mardi ...", 0, 7);
				break;
			case 24:
				processToKeyboard(&jour, "Jours", 0, 31);
				break;
			case 25:
				processToKeyboard(&mois, "Mois", 0, 12);
				break;
			case 26:
				processToKeyboard(&annee, "Annee 2017=17,2018=18", 0, 100);
				break;
			case 27:
				processToKeyboard(&heure, "Heure", 0, 24);
				break;
			case 28:
				processToKeyboard(&minute, "Minute", 0, 59);
				break;
			case 31:
				processToKeyboard(&SOUFFLAGE, "Num sonde soufflage", 0, 3);
				break;
			case 32:
				processToKeyboard(&REPRISE, "Num sonde reprise", 0, 3);
				break;
			case 33:
				processToKeyboard(&EXTRACTION, "Num sonde extraction", 0, 3);
				break;
			case 34:
				processToKeyboard(&AIRNEUF, "Num sonde air neuf", 0, 3);
				break;
			case 35:
				processToKeyboard(&debitMaxCTA, "Debit Max CTA", 0, 20000);
				break;
			case 37:
				processToKeyboard(&consigneTempSoufflageEte, "Cons. Temp. Soufflage ete", 0, 50);
				break;
			case 38:
				processToKeyboard(&consigneTempSoufflageHiver, "Cons. Temp. Soufflage Hiver", 0, 50);
				break;
			case 42:
				processToKeyboard(&consignesTemp[0], "Cons. Temp. Hiver Occ.", 0, 50);
				break;
			case 43:
				processToKeyboard(&consignesTemp[1], "Cons. Temp. Hiver Inocc.", 0, 50);
				break;
			case 44:
				processToKeyboard(&consignesTemp[2], "Cons. Temp. ete Occ.", 0, 50);
				break;
			case 45:
				processToKeyboard(&consignesTemp[3], "Cons. Temp. ete Inocc.", 0, 50);
				break;
			case 48:
				processToKeyboard(&V1.consigneDebitModeOccupe, "Soufflage cons debit Occupe", 0, debitMaxCTA);
				break;
			case 49:
				processToKeyboard(&V1.consigneDebitModeInoccupe, "Soufflage cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 50:
				processToKeyboard(&V1.consigneDebitFreeCooling, "Soufflage cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 62:
				processToKeyboard(&V2.consigneDebitModeOccupe, "Reprise cons debit Occupe", 0, debitMaxCTA);
				break;
			case 63:
				processToKeyboard(&V2.consigneDebitModeInoccupe, "Reprise cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 64:
				processToKeyboard(&V2.consigneDebitFreeCooling, "Reprise cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 53:
				processToKeyboard(&heureDebutOccupe, "Heure debut occupation");
				break;
			case 61:
				processToKeyboard(&heureFinOccupe, "Heure fin occupation");
				break;
			case 54:
				genie.WriteObject(GENIE_OBJ_FORM, FORM::ACCUEIL, 0);
				saveCTA(0);
				//resetFunc();
				break;
			}
			if (KB) {
				if (readIntFromKB) {
					processToKeyboard(int_ptr, param, int_KBmin, int_KBmax);
					readIntFromKB = false;
				}
				else {
					processToKeyboard(ptr, param, double_KBmin, double_KBmax);
				}
				KB = false;
			}
			genie.WriteStr(53, param);
		}
		if (Event.reportObject.object == GENIE_OBJ_KEYBOARD) // If this event is from a Keyboard
		{
			if (Event.reportObject.index == 0)  // If from Keyboard0
			{
				keyboardValue = genie.GetEventData(&Event); // Get data from Keyboard0
				if (keyboardValue == 12) // Valider
				{
					if (form == FORM::DATE_ET_HEURE) modifdate = true;
					if (form == FORM::MENUADMIN) {
						if (str.toInt() != codeAdmin) {
							form = FORM::MENU;
						}
					}
					else {
						if (readIntFromKB) {

							if (str.toInt() >= int_KBmin && str.toInt() <= int_KBmax) {
								*int_ptr = str.toInt();
								erreurKB = false;
								readIntFromKB = false;
							}
							else erreurKB = true;
						}
						else if (readStrFromKB) {
							readStrFromKB = false;
							*string_ptr = str;
							//stringToIp();
						}
						else if (readHeureFromKB) {

							if (checkHeure(str.toInt())) {
								*int_ptr = str.toInt();
								erreurKB = false;
								readHeureFromKB = false;
							}
							else erreurKB = true;
						}
						else {
							if (str.toFloat() >= double_KBmin && str.toFloat() <= double_KBmax) {
								*ptr = str.toFloat();
								erreurKB = false;
							}
							else erreurKB = true;
						}
						changedParams = true;
						if (V1.changedPID) {
							V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);
							V1.changedPID = false;
							//Serial.print("\n\rV1.kp="); Serial.print(V1.Kp);
							//Serial.print("\n\rV1.ki="); Serial.print(V1.Ki);
							//Serial.print("\n\rV1.kd="); Serial.print(V1.Kd);
						}
						if (V2.changedPID) {
							V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
							V2.changedPID = false;
							//Serial.print("\n\rV2.kp="); Serial.print(V2.Kp);
							//Serial.print("\n\rV2.ki="); Serial.print(V2.Ki);
							//Serial.print("\n\rV2.kd="); Serial.print(V2.Kd);
						}
					}
					if (!erreurKB) genie.WriteObject(GENIE_OBJ_FORM, form, 0);

				}
				else if (keyboardValue == 11) //ANNULER
				{
					if (V1.changedPID) V1.changedPID = false;
					if (V2.changedPID) V2.changedPID = false;
					str = "";
					if (form == FORM::MENUADMIN) form = FORM::MENU;
					genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				}
				else if (keyboardValue == 10) //'.'
				{
					str += ".";
					genie.WriteStr(11, str);
				}
				else if (keyboardValue == 13) //DELETE
				{
					str.remove(str.length() - 1);
					genie.WriteStr(11, str);
				}
				else {
					str += keyboardValue;
					genie.WriteStr(11, str);
				}
			}
		}
		saveCTA(0);
	}
}

bool checkHeure(int h) {
	if (String(h).length() > 4 || String(h).length() < 3) return false;
	int H = (int)(h / 100);
	int m = h - H * 100;
	if (H > 23) return false;
	if (m > 59) return false;
	return true;
}

void processToKeyboard(int * ptr, String caption, int min, int max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard(int * ptr, String caption) { //HEURE
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readHeureFromKB = true;
	param = caption;
}
void processToKeyboard(unsigned long * ptr, String caption, int min, int max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = (int*)ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard(double * dptr, String caption, double min, double max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	ptr = dptr;
	param = caption;
	double_KBmin = min;
	double_KBmax = max;
}
void processToKeyboard(String * ptr, String caption) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	string_ptr = ptr;
	param = caption;

	readStrFromKB = true;
}
/*
void stringToIp() {
int point1 = adresseIP.indexOf('.');
int point2 = adresseIP.indexOf('.', point1 + 1);
int point3 = adresseIP.indexOf('.', point2 + 1);
ip[0] = adresseIP.substring(0, point1).toInt();
ip[1] = adresseIP.substring(point1 + 1, point2).toInt();
ip[2] = adresseIP.substring(point2 + 1, point3).toInt();
ip[3] = adresseIP.substring(point3 + 1).toInt();
point1 = passerelle.indexOf('.');
point2 = passerelle.indexOf('.', point1 + 1);
point3 = passerelle.indexOf('.', point2 + 1);
gw[0] = passerelle.substring(0, point1).toInt();
gw[1] = passerelle.substring(point1 + 1, point2).toInt();
gw[2] = passerelle.substring(point2 + 1, point3).toInt();
gw[3] = passerelle.substring(point3 + 1).toInt();
}

void IPToString() {
adresseIP = "";
adresseIP += ip[0]; adresseIP += "."; adresseIP += ip[1]; adresseIP += "."; adresseIP += ip[2]; adresseIP += "."; adresseIP += ip[3];
passerelle = "";
passerelle += gw[0]; passerelle += "."; passerelle += gw[1]; passerelle += "."; passerelle += gw[2]; passerelle += "."; passerelle += gw[3];
}*/


void startScreen() {
	/*4D SYSTEMS*/
	genie.Begin(Serial1);
	genie.AttachEventHandler(myGenieEventHandler);
	pinMode(RESETLINE, OUTPUT);  // Set D4 on Arduino to Output (4D Arduino Adaptor V2 - Display Reset)
	digitalWrite(RESETLINE, 0);  // Reset the Display via D4
	delay(100);
	digitalWrite(RESETLINE, 1);  // unReset the Display via D4
	delay(3500); //let the display start up after the reset (This is important)
	genie.WriteContrast(15);
	waitPeriod = millis();
}



void readMesures() {
	sensors.requestTemperatures();
	if (temperature[0].status == READ_OK)temperature[0].value = sensors.getTempCByIndex(0);
	/*Serial.print(temperature[0].status);
	Serial.print("\t valeur:");
	Serial.print(sensors.getTempCByIndex(0));
	Serial.print("\n\r");*/
	if (temperature[1].status == READ_OK)temperature[1].value = sensors.getTempCByIndex(1);
	/*Serial.print("temp 1\t status:");
	Serial.print(temperature[1].status);
	Serial.print("\t valeur:");
	Serial.print(sensors.getTempCByIndex(1));
	Serial.print("\n\r");*/
	if (temperature[2].status == READ_OK)temperature[2].value = sensors.getTempCByIndex(2);
	///Serial.print(sensors.getTempCByIndex(2));
	//Serial.print("\n\r");
	if (temperature[3].status == READ_OK)temperature[3].value = sensors.getTempCByIndex(3);
	//.print(sensors.getTempCByIndex(3));
	//Serial.print("\n\r");
}

void writeScreen() {
	String timeString = String(jour); timeString.concat("/"); timeString.concat(String(mois)); timeString += "/"; timeString.concat(String(annee)); timeString += " "; timeString.concat(String(heure)); timeString += ":"; timeString.concat(String(minute));
	String modeString = "Mode: ";
	String temp = "";
	String stringdate = "";
	switch (form) {
	case FORM::MENU:
		genie.WriteStr(54, timeString);
		break;
	case FORM::MENUADMIN:
		genie.WriteStr(28, versioncontrollino);
		break;
	case FORM::SYNOPTIQUE:
		if (modeMarche_Applique == modeMarche::ARRET) modeString += "ARRET, ";
		if (modeMarche_Applique == modeMarche::MANU) modeString += "MANU, ";
		if (modeMarche_Applique == modeMarche::AUTO) modeString += "AUTO, ";
		if (modeMarche_Applique == modeMarche::CMDBOUTONS) modeString += "BOITE A BOUTONS, ";
		if (occupe == true) modeString += "Occupe, ";
		else modeString += "Inoccupe, ";
		if (changeOver == true) modeString += "Ete,";
		else modeString += "Hiver,";
		if (modeApplique == HORSGEL)modeString += "\n\r	HorsGel";
		if (modeApplique == FREECOOLING)modeString += "\n\r FREECOOLING";
		if (modeApplique == FREEHEATING)modeString += "\n\r FREEHEATING";
		genie.WriteStr(0, temperature[SOUFFLAGE].value);
		genie.WriteStr(1, temperature[REPRISE].value);
		genie.WriteStr(2, temperature[EXTRACTION].value);
		genie.WriteStr(3, temperature[AIRNEUF].value);
		genie.WriteStr(26, V1.debit);
		genie.WriteStr(25, V2.debit);
		genie.WriteStr(27, RegistreONOFF);
		genie.WriteStr(57, modeString);
		genie.WriteStr(6, modeApplique);
		break;
	case FORM::DATE_ET_HEURE:
		if (jourSemaine == 1) stringdate += "Lundi ";
		if (jourSemaine == 2) stringdate += "Mardi ";
		if (jourSemaine == 3) stringdate += "Mercredi ";
		if (jourSemaine == 4) stringdate += "Jeudi ";
		if (jourSemaine == 5) stringdate += "Vendredi ";
		if (jourSemaine == 6) stringdate += "Samedi ";
		if (jourSemaine == 7) stringdate += "Dimanche ";
		stringdate += String(jour);
		stringdate += " ";
		if (mois == 1) stringdate += "Janvier ";
		if (mois == 2) stringdate += "F�vrier ";
		if (mois == 3) stringdate += "Mars ";
		if (mois == 4) stringdate += "Avril ";
		if (mois == 5) stringdate += "Mai ";
		if (mois == 6) stringdate += "Juin ";
		if (mois == 7) stringdate += "Juillet ";
		if (mois == 8) stringdate += "Ao�t ";
		if (mois == 9) stringdate += "Septembre ";
		if (mois == 10) stringdate += "Octobre ";
		if (mois == 11) stringdate += "Novembre ";
		if (mois == 12) stringdate += "Decembre ";
		stringdate += "20";
		stringdate += String(annee);
		genie.WriteStr(7, stringdate);
		genie.WriteObject(GENIE_OBJ_CUSTOM_DIGITS, 0, heure);
		genie.WriteObject(GENIE_OBJ_CUSTOM_DIGITS, 1, minute);
		break;
	case FORM::PARAMS_VENTILO:
		genie.WriteObject(GENIE_OBJ_LED, 0, (int)V1.defaut);
		genie.WriteObject(GENIE_OBJ_LED, 1, (int)V2.defaut);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 0, (int)V1.commandeMarche);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 5, (int)V2.commandeMarche);
		genie.WriteStr(12, V2.consigne_m3h);
		genie.WriteStr(10, V1.consigne_m3h);
		genie.WriteStr(13, V1.Kp);
		genie.WriteStr(14, V1.Ki);
		genie.WriteStr(15, V1.Kd);
		genie.WriteStr(16, V1.Kfactor);
		genie.WriteStr(17, V2.Kp);
		genie.WriteStr(18, V2.Ki);
		genie.WriteStr(19, V2.Kd);
		genie.WriteStr(20, V2.Kfactor);
		genie.WriteStr(21, V1.debit);
		genie.WriteStr(22, V1.cons_app_pc);
		genie.WriteStr(23, V2.debit);
		genie.WriteStr(24, V2.cons_app_pc);
		break;
	case FORM::PARAMS_RM:
		break;
	case FORM::BOUTONS:
		genie.WriteObject(GENIE_OBJ_USER_LED, 1, Ventilodefaut);
		genie.WriteObject(GENIE_OBJ_USER_LED, 0, bpmarche);
		genie.WriteStr(6, consigne_m3h);
		break;
	case FORM::CONFIG_CTA:
		genie.WriteStr(9, SOUFFLAGE);
		genie.WriteStr(33, REPRISE);
		genie.WriteStr(34, EXTRACTION);
		genie.WriteStr(35, AIRNEUF);
		genie.WriteStr(36, debitMaxCTA);
		genie.WriteStr(52, seuilTempHorsGel);
		break;
	case FORM::PARAMS_CTA:
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 4, (int)!regulTempReprise);
		genie.WriteObject(GENIE_OBJ_DIPSW, 0, (int)modeMarche_Applique);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 3, (int)changeOver);
		genie.WriteStr(37, consigneTempSoufflageEte);
		genie.WriteStr(38, consigneTempSoufflageHiver);
		genie.WriteStr(39, seuilTempHorsGel);
		break;
	case FORM::PARAMS_CTA2:
		genie.WriteStr(40, consignesTemp[0]);
		genie.WriteStr(41, consignesTemp[1]);
		genie.WriteStr(42, consignesTemp[2]);
		genie.WriteStr(43, consignesTemp[3]);
		genie.WriteStr(44, V1.consigneDebitModeOccupe);
		genie.WriteStr(45, V1.consigneDebitModeInoccupe);
		genie.WriteStr(46, V1.consigneDebitFreeCooling);
		genie.WriteStr(49, V2.consigneDebitModeOccupe);
		genie.WriteStr(50, V2.consigneDebitModeInoccupe);
		genie.WriteStr(51, V2.consigneDebitFreeCooling);
		break;
	case FORM::PARAMS_CTA3:
		if (String(heureDebutOccupe).length() == 4) {
			temp = String(heureDebutOccupe).substring(0, 2); temp += ":"; temp += String(heureDebutOccupe).substring(2);
		}
		else {
			temp = String(heureDebutOccupe).substring(0, 1); temp += ":"; temp += String(heureDebutOccupe).substring(1);
		}
		genie.WriteStr(47, temp);
		if (String(heureFinOccupe).length() == 4) {
			temp = String(heureFinOccupe).substring(0, 2); temp += ":"; temp += String(heureFinOccupe).substring(2);
		}
		else {
			temp = String(heureFinOccupe).substring(0, 1); temp += ":"; temp += String(heureFinOccupe).substring(1);
		}
		genie.WriteStr(48, temp);
		for (int i = 0; i < 7; i++) {
			genie.WriteObject(GENIE_OBJ_DIPSW, i + 1, (int)jourOccupe[i]);
		}
		break;
	}

}

/**
* Fonction de lecture de la temp?rature via un capteur DS18B20.
*/
/*
byte getTemperature(int sensorId) {
//sensors.requestTemperatures();
//byte data[9];
/* Reset le bus 1-Wire et s?lectionne le capteur */
//ds.reset();
//ds.select(temperature[sensorId].addr);
/* Lance une prise de mesure de temp?rature et attend la fin de la mesure */
//ds.write(0x44, 1);
//delay(50);
/* Reset le bus 1-Wire, s?lectionne le capteur et envoie une demande de lecture du scratchpad */
//ds.reset();
//ds.select(temperature[sensorId].addr);
//ds.write(0xBE);
/* Lecture du scratchpad */
//for (byte i = 0; i < 4; i++) {
//data[i] = ds.read();
//Serial.println(sensors.getTempCByIndex(sensorId));
/*Serial.print("\n\r i=");
Serial.print(i);
Serial.print("data");
Serial.print(data[i]);*/
//}
/* Calcul de la temp?rature en degr? Celsius */
//temperature[sensorId].value = ((data[1] << 8) | data[0]) * 0.0625;
//temperature[sensorId].value = sensors.getTempCByIndex(sensorId);
// Pas d'erreur
//Serial.print("\n\r value:");
//Serial.print(temperature[sensorId].value);
//Serial.print("\n\r okkkkkay");
//return READ_OK;
//}

/**
* Fonction de lecture de la temp?rature via un capteur DS18B20.
*/
byte getDS18B20Addresses(int nbSensors) {
	ds.reset_search();

	for (int i = 0; i < nbSensors; i++) {
		/* Recherche le prochain capteur 1-Wire disponible */
		if (!ds.search(temperature[i].addr)) {
			// Pas de capteur
			temperature[i].status = NO_SENSOR_FOUND;
			Serial.print("temperature[i].status = NO_SENSOR_FOUND");
			Serial.println(temperature[i].addr[0]);
		}
		// V?rifie que l'adresse a ?t? correctement re?ue 
		if (OneWire::crc8(temperature[i].addr, 7) != temperature[i].addr[7]) {
			// Adresse invalide
			temperature[i].status = INVALID_ADDRESS;
			Serial.print("temperature[i].status = INVALID_ADDRESS");
		}
		// V?rifie qu'il s'agit bien d'un DS18B20 
		if (temperature[i].addr[0] != 0x28) {
			// Mauvais type de capteur
			temperature[i].status = INVALID_SENSOR;
			Serial.print("temperature[i].status = INVALID_SENSOR");
		}
		//Serial.print(i); Serial.print(":"); Serial.println(temperature[i].status); Serial.print(":"); Serial.println(temperature[i].addr[0]);
		temperature[i].status = READ_OK;
	}
}



